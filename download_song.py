# This requires some libraries:
# pip install mutagen
# pip install yt-dlp
# pip install future
# pip install Pillow
#
# Get ffmpeg, dump it's binaries in Python27/scripts: https://stackoverflow.com/questions/30770155/ffprobe-or-avprobe-not-found-please-install-one/38878753#38878753

from datetime import datetime

import subprocess, sys, urllib.parse, os, threading, queue, base64, urllib, re, io
from urllib.request import urlopen, HTTPError
import tkinter as tk
from tkinter import messagebox
from mutagen.oggopus import OggOpus
from mutagen.oggvorbis import OggVorbis
from mutagen.flac import Picture
from PIL import Image

try:
    from pip import main as pipmain
except:
    from pip._internal import main as pipmain
    
# Use with unicode translate to remove forbidden characters for Windows filenames.
FORBIDDEN_CHARACTERS_TRANSLATION_TABLE = dict.fromkeys(map(ord, '*<>:"\\|?*'), None)
    
class App(tk.Tk, object):

    def __init__(self):
        super(App, self).__init__()
        
        self.wm_title("Song Downloader (with Album Art) (.opus) (Youtube)") 
        self.geometry("600x150+400+300")
        self.iconbitmap("song_downloader_Nvn_icon.ico")        
        
        self.downloading_count = 0
        self.finished_downloading_count = 0
        
        for path in ["temp", "outputs"]:
            if not os.path.exists(path):
                os.makedirs(path)
        
        tk.Label(self, text="Input Link Below:").grid(row=0)
        
        self.link_textfield = tk.Entry(self, width=60)
        self.link_textfield.grid(row=1, padx=(20,0))
        
        tk.Button(self, text="Download", command=self.download_song_with_art_in_thread, padx=15, pady=15, bg="#BCED91", 
                  font=("Verdana", 15, "bold")).grid(row=1, column=1, rowspan=5, sticky="NE", padx=5)
        
        self.hidden_status_message = tk.Label(self)
        self.hidden_status_message.grid(row=3)
        
        self.downloading_label = tk.Label(self, text="Currently Downloading: 0")
        self.downloading_label.grid(row=4)
        
        self.finished_downloading_label = tk.Label(self, text="Finished Downloading: 0")
        self.finished_downloading_label.grid(row=5)
        
        self.bind_all("<Control-a>", self.select_all)
        self.bind_all("<Return>", self.download_song_with_art_in_thread)
        
        bottom_button_frame = tk.Frame(self)
        bottom_button_frame.grid(row=6, sticky="SW", pady=(10,0), padx=5)
        
        tk.Button(bottom_button_frame, text="Open output folder", command=self.open_output_folder, bg="#BCED91", 
                  font=("Verdana", 8,)).grid(row=0, column=0, sticky="SW", pady=(10,0))
        
        tk.Button(bottom_button_frame, text="Update yt-dlp", command=self.update_yt_dlp, bg="#BCED91", 
                  font=("Verdana", 8,)).grid(row=0, column=1, sticky="SW")        
        
        self.link_textfield.focus()
        
    def select_all(self, event):
        event.widget.select_range(0,tk.END)
        
    def update_status_labels(self):
        self.downloading_label.configure(text="Currently Downloading: " + str(self.downloading_count))
        self.finished_downloading_label.configure(text="Finished Downloading: " + str(self.finished_downloading_count))
        
    def download_song_with_art_in_thread(self, event=None):
        link = self.link_textfield.get()
        
        # Don't download playlists.
        link = link.split("&list=")[0]
        
        if not link:
            self.show_error_message("Error", "Please input a youtube link.")
            return 0
        
        if messagebox.askokcancel("Confirm", "Download "+link+"?"):
            self.downloading_count += 1
            self.update_status_labels()
            
            thread_queue = queue.Queue()
            
            self.new_thread = threading.Thread(target = self.download_song_with_art, args=(thread_queue, link))
            self.new_thread.start()
            
            self.check_for_processing_completion(thread_queue)
        
    def download_song_with_art(self, thread_queue, link):
        start_time = datetime.now()
    
        video_id = self.get_video_id_from_url(link)
        
        for file_name in os.listdir(u"./outputs"):
            if video_id in file_name:
                thread_queue.put("ErrorMessage=Song already downloaded.\n\nDownload canceled.")
                return 0
        
        self.download_song(link)
        
        art_file_path = self.download_album_art(video_id)
        
        audio_file_path = self.embed_album_art(video_id, art_file_path)
        
        thread_queue.put("Complete")
        
        print("Complete. | Time elapsed:", datetime.now() - start_time)
       
    def get_video_id_from_url(self, link):
        url_data = urllib.parse.urlparse(link)
        query = urllib.parse.parse_qs(url_data.query)
        video_id = query["v"][0]
        
        return video_id       
        
    def download_song(self, link):
        #command = "youtube-dl --extract-audio --audio-format opus --audio-quality 0 -o " + \
            #os.path.abspath("outputs\%(title)s-%(id)s.%(ext)s") + " " + link
        command = ["yt-dlp", "--extract-audio", "--audio-format", "opus", "--ffmpeg-location", "C:/ProgramData/chocolatey/lib/ffmpeg/tools/ffmpeg/bin",
                   "--audio-quality", "0", "-o", os.path.abspath("outputs\%(title)s-%(id)s.%(ext)s"), link]
        subprocess.call(command)
        
    def download_album_art(self, video_id):
        
        output_file_name = ""
        
        for resolution in ["maxresdefault", "hqdefault", "0", "mqdefault", "default"]: #Ordered from highest to lowest
            thumbnail_link = "https://img.youtube.com/vi/"+video_id+"/"+resolution+".jpg"
            try:
                response = urlopen(thumbnail_link)
                html = response.read()
                output_file_name = resolution + ".jpg" 
                with open("temp/" + output_file_name, "wb") as output_f:
                    output_f.write(html)
                print("Downloaded '" + resolution + "'")
                break
            except HTTPError:
                print("No '" + resolution + "'") #Try the next resolution
                
        if len(output_file_name) > 0:
            return "temp/" + output_file_name
        else:
            raise RuntimeError("Error: Failed to download album art.")
        
    def embed_album_art(self, video_id, art_file_path):
        audio_file_path = ""
        
        for file_name in os.listdir(u"./outputs"):
            if video_id in file_name:
                audio_file_path = "./outputs/"+file_name
                try:
                    print("Found audio file:", audio_file_path.encode('ascii', 'ignore'))
                except:
                    print("Warning: Failed to encode audio_file_path for printing.")
                break
            
        if len(audio_file_path): 
            if audio_file_path.endswith(".opus"):
                audio = OggOpus(audio_file_path)
            elif audio_file_path.endswith(".ogg"):
                audio = OggVorbis(audio_file_path)
            else: 
                raise NotImplementedError("Embedding album art for formats other than .opus is not implemented.")
            
            im = Image.open(art_file_path)
            width, height = im.size
            with open(art_file_path, "rb") as c:
                data = c.read()
            picture = Picture()
            picture.data = data
            picture.type = 17
            picture.desc = u"Cover Art"
            picture.mime = u"image/jpeg"
            picture.width = width
            picture.height = height
            picture.depth = 24
            picture_data = picture.write()
            encoded_data = base64.b64encode(picture_data)
            vcomment_value = encoded_data.decode("ascii")
            audio["metadata_block_picture"] = [vcomment_value]
            audio.save()            
        else:
            raise RuntimeError("Error: Could not find video file with id: " + video_id)
        
        return audio_file_path
        
    def show_error_message(self, title, message, *args, **kwargs):
        '''Wrapper of messagebox.showerror()'''
        messagebox.showerror(title, message, *args, **kwargs)    
        
    def check_for_processing_completion(self, thread_queue):
        '''Periodically check for if processing is complete.
        
        Go through the queue completely each time to prevent showing out-dated status.
        '''
        
        try:
            while(1):
                status = thread_queue.get(0) 
                
                if status == "Complete" or status == "Error":
                    self.downloading_count = self.downloading_count - 1
                    
                    del thread_queue # Prevent zombies by deleting multiprocessing queue.
                    if status == "Complete":
                        self.finished_downloading_count += 1
                        self.update_status_labels()
                        return 1
                    else:
                        self.update_status_labels()
                        return 0
                elif status.startswith("ErrorMessage="):
                    self.downloading_count = self.downloading_count - 1
                    self.update_status_labels()    
                    self.show_error_message("Error", status[len("ErrorMessage="):], parent=self)
                else:
                    self.set_hidden_status_message(status, color="#424949")
        except queue.Empty as e:
            self.after(1000, self.check_for_processing_completion, thread_queue)
            
    def open_output_folder(self):
        os.startfile(os.path.realpath("./outputs"))
    
    def update_yt_dlp(self):
        pipmain(["install", "--upgrade", "yt-dlp"])
        
if __name__ == "__main__":
    app = App()
    app.mainloop()
    
    #video_id = "_Yhyp-_hX2s"
    
    #art_file_path = app.download_album_art(video_id)
    #app.embed_album_art(video_id, art_file_path)