# SIVLEOL's youtube music downloader #

### Description ###

A youtube-dl wrapper that downloads music from youtube, embeds album art and renames the final result to have unicode title. (ie. Japanese characters work)

Provides a simple GUI. Supports parallel downloads. Ctrl-A can be used any time to select all the text in the textfield.

Downloads ".opus" files using youtube-dl, then downloads their album art from youtube and embeds it.

### Requirements ###

python 3.7+

Install some libraries:

* pip install mutagen

* pip install yt-dlp

* pip install future

* pip install Pillow

ffmpeg is required, yt-dlp has some links to modified versions of it on their git. 
Alternatively, use chocolately to install ffmpeg: choco install ffmpeg
See: https://jcutrer.com/windows/install-chocolatey-choco-windows10

Then use: "--ffmpeg-location" "C:/ProgramData/chocolatey/lib/ffmpeg/tools/ffmpeg/bin"
(or whatever the location is)

### Usage ###
Run "python download_song.py" to get the GUI, after everything is installed.

![Screenshot](https://bitbucket.org/SIVLEOL/sivleol-youtube-music-downloader/raw/f7038e5e450b85ddab4965b4715879101975f3b2/screenshots/screenshot.jpg)

Paste youtube link in, then press download.

Once download is done, .opus file will be in output folder.

For ease of use, Ctrl-A can be used to select all the text in the field for links. (to delete and paste another link)
